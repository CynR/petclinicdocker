FROM maven:3.6.3-jdk-8 as build
WORKDIR /src
COPY pom.xml pom.xml
RUN mvn dependency:go-offline
COPY . .
RUN mvn spring-javaformat:apply
RUN mvn package

FROM java
COPY --from=build /src/target/*.jar .

EXPOSE 8080
CMD java -jar *.jar